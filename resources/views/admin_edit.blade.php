@extends('admin')




@section('content')






        <div class="row">

            <div class="col-12">

                {!! Form::open(array('url'=>route('news.update' , array('news'=>$single_news->id , 'locale'=>request()->route()->parameters['locale'])) , 'method'=>'PUT')) !!}

                <div class="form-group">

                    <label> {!! __("admin_news.title") !!} </label>

                    {!! Form::text('title',old('title',$single_news->title),array('class'=>'form-control')) !!}


                </div>

                <div class="form-group">


                    <label> {!! __("admin_news.description") !!} </label>
                    {!!  Form::textarea('description', old('description' , null) , array('class'=>'form-control') ) !!}




                </div>





                <div class="form-group">

                    <label> {!! __("admin_news.content") !!} </label>

                    {!!  Form::textarea('content', old('content',$single_news->content) , array('id'=>'content') ) !!}



                </div>

                <button type="submit" class="btn btn-primary mb-4">{!! __('general.update-button-text') !!}</button>

                {!! Form::close() !!}



            </div>




        </div>




@endsection






@section('scripts')

    {!! Html::script('js/tinymce/js/tinymce/tinymce.js') !!}




    <script>
        var editor_config = {
            path_absolute : "/",
            selector: "#content",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            }
        };

        tinymce.init(editor_config);
    </script>﻿







@stop



