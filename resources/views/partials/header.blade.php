





<div class="container-fluid px-0" >

<div class="row">
    <header class="col-12">
    <nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">{!!  mb_convert_case(__('general.navigation'), MB_CASE_TITLE, "UTF-8") !!}</a>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="{{route('news.index',['locale'=>app()->getLocale()])}}">{!! mb_convert_case(__('admin_news.all-news'), MB_CASE_TITLE, "UTF-8") !!} <span class="sr-only">(current)</span></a>
            </li>

        </ul>

    </div>
</nav>
</header>
</div>
</div>