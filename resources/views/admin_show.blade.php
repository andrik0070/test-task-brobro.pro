@extends('admin')

@section('content')







        @if(Session::has('success-message-created'))





        <div class="row">

            <div class="col-md-10 offset-md-1 ">

                <div class="alert alert-success mt-3" role="alert">

                   {!! __('admin_news.success-message-created') !!}


                </div>


            </div>

        </div>


        @elseif(Session::has('success-message-updated'))


            <div class="row">

                <div class="col-md-10 offset-md-1">

                    <div class="alert alert-success mt-3" role="alert">

                        {!! __('admin_news.success-message-updated') !!}



                    </div>


                </div>

            </div>




        @endif



        <div class="row">


            <div class="col-md-8 offset-md-2 mt-4">

                <h2 class="mb-3">{{$single_news->title}}</h2>
                <div class="mb-3">

                  {{ $single_news->short_description }}

                </div>

                <div class="">

                    {!! $single_news->content !!}


                </div>



                <div class="row">


                   <div class="col-md-6 offset-md-6">

                       <div class="row">

                       <div class="col-md-1 offset-md-4">
                    <a href="{{route('news.edit',array('news'=>$single_news,'locale'=>request()->route()->parameters['locale']))}}"><button class="btn btn-primary">{!! __('general.edit-button-text') !!}</button></a>
                    </div>
                    <div class="{{ $deleteButtonDivOffset }} col-md-1">
                        {!! Form::open(array('url'=>route('news.destroy', array('news'=>$single_news,'locale'=>request()->route()->parameters['locale'])) , 'method'=>'DELETE')) !!}

                        {!! Form::submit(__('general.delete-button-text'),array('class'=>'btn btn-danger')) !!}



                        {!! Form::close() !!}
                    </div>
                       </div>

                   </div>
                </div>


            </div>


        </div>









@stop