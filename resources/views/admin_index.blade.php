@extends('admin')




@section('content')




    @foreach($news as $single_news)

    <div class="row">

        <div class="col-md-8 offset-md-2">

            <h3 class="mt-3"> <a href="{{ route('news.show' , ['news'=>$single_news , 'locale'=>app()->getLocale()]) }}" > {{ $single_news->title }}  </a> </h3>

            <div class="">

                {{ $single_news->short_description }}

            </div>

        </div>

    </div>

        <div class="row">

            <div class="col-md-5 offset-md-7">

              <div class="row">
                  <div class="col-md-1 offset-md-2 ">
                <a href="{{route('news.edit',['news'=>$single_news , 'locale'=>app()->getLocale()])}}"><button class="btn btn-primary">{{__('general.edit-button-text')}}</button></a>
                </div>
            <div class="col-md-1 {{$deleteButtonDivOffset}}">
                {!! Form::open(array('url'=>route('news.destroy', ['news'=>$single_news , 'locale'=>app()->getLocale()]) , 'method'=>'DELETE')) !!}

                {!! Form::submit(__('general.delete-button-text'),array('class'=>'btn btn-danger')) !!}



                {!! Form::close() !!}
            </div>

              </div>

            </div>

        </div>

   @endforeach





@stop












