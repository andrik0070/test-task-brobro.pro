<!doctype html>
<html lang="en">

<head>


    @include('partials.head')



    {!!  Html::style('css/style.css') !!}
    {!!  Html::style('css/bootstrap.min.css') !!}


    @section('styles')



    @show



</head>


<body>

@include('partials/header')


<div class="container-fluid" id="content-container">


    @section('content')



    @show

</div>


@include('partials/footer')


@section('scripts')



@show


</body>

</html>