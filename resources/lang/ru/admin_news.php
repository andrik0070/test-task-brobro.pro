<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 17.8.9
 * Time: 13:24
 */


return
	[
		'success-message-created' =>  'Новость была  <strong>успешно</strong> создана',
		'success-message-updated' =>  'Новость была <strong>успешно</strong> обновлена ',
		'title'=>'Заголовок',
		'description'=>'Описание',
		'content'=>'Содержание',
		'all-news'=>'Все новости'



	];