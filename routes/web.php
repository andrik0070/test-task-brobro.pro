<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix'=>'admin/{locale}' , 'middleware'=>['auth' , 'bindings'] ],
function()
{


	Route::get('news/{news}' , 'NewsController@show')->middleware('can:view,news')->name('news.show');

	Route::get('news/' , 'NewsController@index')->middleware('can:view_all,App\News')->name('news.index');

	Route::get('news/create' , 'NewsController@create')->middleware('can:create,App\News')->name('news.create');

	Route::post('news/' , 'NewsController@store')->middleware('can:create,App\News')->name('news.store');

	Route::get('news/{news}/edit' , 'NewsController@edit')->middleware('can:update,news')->name('news.edit');

	Route::put('news/{news}' , 'NewsController@update')->middleware('can:update,news')->name('news.update');

	Route::delete('news/{news}' , 'NewsController@destroy')->middleware('can:delete,news')->name('news.destroy');
}


);



//Route::get('set_admin' , 'NewsController@setAdmin');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
