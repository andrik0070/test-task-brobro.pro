<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreNews;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\News;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\App;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */


	public function index()
    {
    	$news = News::all();

	    if(App::getLocale()=="ru" )
	    {

		    $deleteButtonDivOffset = "offset-md-3";

	    }
	    else if(App::getLocale()=="lv")
	    {
		    $deleteButtonDivOffset = "offset-md-2";
	    }
	    else
	    {
		    $deleteButtonDivOffset = "offset-md-1";
	    }



    	return view('admin_index',['news'=>$news , 'deleteButtonDivOffset'=>$deleteButtonDivOffset]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


    	return view('admin_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($locale , Request $request , StoreNews $news_request )
    {
        $single_news = new News;
        $single_news->title = $request->input('title');
        $single_news->short_description = $request->input('description');
        $single_news->content = $request->input('content');

        $single_news->save();


        $request->session()->flash('success-message-created', true);

         return redirect()->route('news.show', ['news'=> $single_news , 'locale'=>$locale]); ;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale,News $news)
    {

        if(App::getLocale()=="ru" )
        {

	        $deleteButtonDivOffset = "offset-md-4";

        }
        else if(App::getLocale()=="lv")
        {
	        $deleteButtonDivOffset = "offset-md-2";
        }
        else
        {
	        $deleteButtonDivOffset = "offset-md-1";
        }
    	
    	return view('admin_show',array('single_news'=>$news , "deleteButtonDivOffset"=>$deleteButtonDivOffset));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale , News $news)
    {



    	return view('admin_edit' , array('single_news' => $news ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($locale, News $news ,Request $request , StoreNews $news_request)
    {


	    $news->title = $request->input('title');
	    $news->short_description = $request->input('description');
	    $news->content = $request->input('content');

	    $news->save();

	    $request->session()->flash('success-message-updated', true);

	    return redirect()->route('news.show', ['news'=>$news,'locale'=>$locale]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale , News $news)
    {

        $news->delete();

	    return redirect()->route('news.index' , ['locale'=>$locale]);
    }

    public function setAdmin()
    {




    	$admin = User::find(1);

    	var_dump($admin->can('news.create'));





    }
}
